import algorithm.CheckPrime;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {


        for (int i = 0; i < 4; i++) {


            long start = System.nanoTime();

            try (Stream<String> candidates = Files.lines(Paths.get("src/main/resources/numbers.txt"))) {
                int sumOfPrimes = candidates
                        .parallel()
                        .mapToLong(l -> Long.valueOf(l))
                        .filter(l -> CheckPrime.isPrime(l))
                        .mapToInt((l) -> 1)
                        .sum();

                System.out.println("Parallel version found: " + sumOfPrimes + " primes.");

            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Parallel execution took: " + String.valueOf((System.nanoTime() - start) / 1_000_000_000) + "s.");
            System.out.println("----------------------------------------");


            long start2 = System.nanoTime();

            try (Stream<String> candidates = Files.lines(Paths.get("src/main/resources/numbers.txt"))) {
                int sumOfPrimes = candidates
                        .mapToLong(l -> Long.valueOf(l))
                        .filter(l -> CheckPrime.isPrime(l))
                        .mapToInt((l) -> 1)
                        .sum();

                System.out.println("Concurrent version found: " + sumOfPrimes + " primes.");

            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Concurrent execution took: " + String.valueOf((System.nanoTime() - start2) / 1_000_000_000) + "s.");
            System.out.println("----------------------------------------");


        }

    }
}
