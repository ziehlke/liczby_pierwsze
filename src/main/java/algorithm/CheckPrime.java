package algorithm;

public class CheckPrime {
    public static boolean isPrime(long l) {
        if (l < 2) return false;

        for (int i = 2; i < l; i++) {
            if (l % i == 0) return false;
        }
        return true;
    }
}
